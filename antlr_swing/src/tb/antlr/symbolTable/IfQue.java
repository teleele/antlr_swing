package tb.antlr.symbolTable;

import java.util.ArrayDeque;
import java.util.Deque;

public class IfQue {
	Deque<Integer> ifstat = new ArrayDeque<>();
	Integer ifstat_cnt;
	
	public IfQue() {
		ifstat_cnt = 0;
	}
	
	public Integer enterIfStat() {
		ifstat.push(ifstat_cnt);
		ifstat_cnt++;
		return ifstat.size();
	}
	
	public Integer leaveIfStat() throws RuntimeException {
		if (ifstat.size() < 0)
			throw new RuntimeException("Cannot exit more if statements!");
		ifstat.remove();
		return ifstat.size();
	}
	
	public Integer getIfStat() {
		return ifstat.peek();
	}
	
	public Integer getIfStatDepth() {
		return ifstat.size();
	}
	
	public Integer getIfStatMax() {
		return ifstat_cnt;
	}
}
