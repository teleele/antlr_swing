grammar Expr;

options {
  output=AST;
  ASTLabelType=CommonTree;
}

@header {
package tb.antlr;
}

@lexer::header {
package tb.antlr;
}

prog
    : (stat | block)+ EOF!;

stat
    : expr NL -> expr
    | VAR ID (WRITE expr)? NL -> ^(VAR ID) ^(WRITE ID expr)?
    | ID WRITE expr NL -> ^(WRITE ID expr)
    | ifstat NL -> ifstat
    | NL ->
    ;

expr
    : multExpr
      ( AR_ADD^ multExpr
      | AR_SUB^ multExpr
      )*
    ;
 
ifstat
    : IF^ ifexpr THEN! NL? block (ELSE! NL? block)?
    ;
     
ifexpr
    :  expr (
              REL_E^  expr
            | REL_NE^ expr
            | REL_G^  expr
            | REL_GE^ expr
            | REL_L^  expr
            | REL_LE^ expr
            )*  
    ;

multExpr
    : atom
      ( AR_MUL^ atom
      | AR_DIV^ atom
      )*
    ;

atom
    : INT
    | ID
    | LP! expr RP!
    ;
    
block
    : BRL^ (stat | block)* BRR!
    ;

VAR : 'var';

IF  : 'if';

ELSE: 'else';

THEN: 'then';

ID : ('a'..'z'|'A'..'Z'|'_') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*;

INT : '0'..'9'+;

NL : '\r'? '\n' ;

WS : (' ' | '\t')+ {$channel = HIDDEN;} ;


LP
	:	'('
	;

RP
	:	')'
	;

WRITE
	:	'='
	;
  
AR_ADD
	:	'+'
	;

AR_SUB
	:	'-'
	;

AR_MUL
	:	'*'
	;

AR_DIV
	:	'/'
	;
	
BRL
  : '{'
  ;
   
BRR
  : '}'
  ;
  
REL_E
  : '=='
  ;
REL_NE
  : '!='
  ;
REL_G
  : '>'
  ;
REL_GE
  : '>='
  ;
REL_L
  : '<'
  ;
REL_LE
  : '<='
  ;
